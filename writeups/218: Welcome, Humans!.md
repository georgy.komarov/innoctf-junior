# Welcome, Humans! (100)

> Добро пожаловать на сайт нашего турнира, уважаемые участники! Располагайтесь удобнее, пользуйтесь интерфейсом. Главное, помните - не будьте роботами!!!

**Tags**: web, site, recon

# Write-up
1. Robots triggered xD
2. Get your flag `https://innoctf.hackforces.com/robots.txt`

**Flag**: `CTF{w3lc0m3_r0b0t5_t00}`

**Solved by**: `Georgy`
