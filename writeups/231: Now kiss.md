# Now kiss (400)

> We managed to get a dump of memory. Find out what was going on.
https://yadi.sk/d/9dA7TnLd3U6JMf

**Tags**: forensics

# Write-up
`strings -e l test4 | grep CTF{`

Fixed after reporting to orgs

**Normal solution TODO**

**Flag**: `CTF{Th1s_15_re@l_Flaag}`

**Solved by**: `Georgy & Ruslan`
