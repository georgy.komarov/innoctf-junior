# Txt it! (100)

> А что еще ты можешь выяснить про наш сайт? 

> Хинты: NET, флаг лежит не на нашем сервере, делается минимум в 46 символов, Yandex рулит, Мы заливаем файлы на Яндекс.Диск, мы используем API переводчика, что ещё мы можем использовать? 

**Tags**: net, web, recon, site

# Write-up
**.... no comments ....**

*Solved after hints*

`dig @dns2.yandex.net -t txt innoctf.hackforces.com`

**Flag**: `CTF{DN5_R3CORDS_FOR_HUMAN_R3DABLE_TXT}`

**Solved by**: `Ruslan`

**Writeup by**: `Georgy`