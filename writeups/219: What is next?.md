# What is next? (100)

> Наши специалисты получили сигнал с неизвестного обьекта. Ваша задача понять что внутри.
https://innoctf.hackforces.com/files/crypto/100/task

> p.s. не забудьте обернуть флаг в  CTF{}

**Tags**: crypto, encode, matryoshka

# Write-up
1. ASCII encode and base64 decode
    ```python
    from base64 import b64decode

    data = open('task').read().strip().split()
    result = ''
    for i in data:
        result += chr(int(i, 2))
    
    decoded = b64decode(result).decode().strip()
    print(decoded)
    ```
2. Decode Morse --> LBHARRQGBTBVAFVQR
3. ROT13 --> YOUNEEDTOGOINSIDE
4. Add CTF{}

**Flag**: `CTF{YOUNEEDTOGOINSIDE}`

**Solved by**: `Georgy`
