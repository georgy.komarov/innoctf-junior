# What is inside? (200)

> Еще одно послание от неизвестных. Возможно в нем есть флаг, но это не точно. 

https://innoctf.hackforces.com/files/misc/200/task

**Tags**: madness

# Write-up
1. Decode base64
2. It seems to be encrypted .zip archive
3. Bruteforce the password with John the Ripper
    ```
    zip2jonh decoded.zip > decoded.hash
    john --wordlist=rockyou.txt decoded.hash
    john --show decoded.hash
    ```
    Password is `73313`
4. Decrypt .zip and find the flag

**Flag**: `CTF{Th1s_1s_ur_flaaaaag}`

**Solved by**: `Georgy`
