# What the file? (300)

> Мы нашли файл от старого админа, но не можем его открыть. Есть идеи, как это сделать?
https://innoctf.hackforces.com/files/admin/300/file

**Tags**: admin, file, matryoshka

# Write-up
В Manjaro встроенный архиватор прекрасно справляется с таском
1. Use Ark
2. Use Ark
3. Use Ark
4. Use Ark
5. Use Ark
6. Find FLAG.txt

**Flag**: `CTF{z1pp3d_h4ck3d}`

**Solved by**: `Georgy`
