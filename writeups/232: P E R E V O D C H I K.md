# P E R E V O D C H I K (300)

> Как у вас с английским? А с немецким, французским? Сейчас узнаем. Заходите и помогайте нашему боту переводить слова на различные языки. Щедрая награда в виде флага ожидает вас после 100 успешных переводов подряд.

nc tcp.innoctf.hackforces.com 5061

P.S. Yandex рулит

**Tags**: ppc, tcp, translate

# Write-up
**TODO**

**Flag**: `CTF{programmist_maminoy_podrugi_v2.0}`

**Solved by**: `Semyon & Ruslan`