# Do u now dewei? (200)

> Получен сигнал от пришельцев! Что же это может быть? кажется мы начинаем их понимать....
Лингвисты распознали синтксис: https://innoctf.hackforces.com/files/ppc/200/syntax.txt
https://innoctf.hackforces.com/files/ppc/200/task

**Tags**: ppc

1. Видим что синтаксис кода похож на BrainFuck
2. Заменяем в коде все команды на аналогичные в BrainFuck
3. Запускаем в интерпритаторе BF
4. PROFIT!

# Write-up
**TODO**

**Flag**: `CTF{DO_Y0U_L1ke_Nuc1es}`

**Solved by**: `Semyon`