# Can't press this (200)

> Не нажимать. ну вы знаете.

http://tcp.innoctf.hackforces.com:5001

**Tags**: web

# Write-up
Found
http://tcp.innoctf.hackforces.com:5001/doc/dic.txt in **JS Source**

Seems to be fixed now...

**Flag**: `CTF{I_also_l1ke_r3d_button5}`

**Solved by**: `Georgy`
