# Might Be One Time Ipad (300)

> Канал общения преступников был скомпроментирован. Схема шифрования была получена с компьютеров злоумышленников. В начале апреля мы перехватили это сообщение. Ваша задача - расшифровать перехваченное сообщение!

https://innoctf.hackforces.com/files/crypto/300/encrypt.py
https://innoctf.hackforces.com/files/crypto/300/task

**Tags**: crypto

# Write-up


**Flag**: `CTF{xor_be_0R_not_xor_b3}`

**Solved by**: `Nikolay`
